﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Tablic.Classes;

namespace Tablic {
    
    public class Stash : CardCollection {

        private Form1 ParentForm { get; set; }
        private Card Played { get; set; }

        public Stash(Form1 parent) : base() {
            ParentForm = parent;
        }

        public bool IsCorrect(Card played) {

            List<int> arr = TransMutate();
            Played = played;
            try {
                Fork(arr, 0);
                return false;
            }
            catch (Exception exc) {
                if (exc.Message == "permutation found!")
                    return true;
                else
                    throw exc;
            }
        }

        private void Fork(List<int> arr, int pos) {


            if (arr[pos] == 1) {
                List<int> arr_left  = new List<int>(arr);
                List<int> arr_right = new List<int>(arr);

                arr_right[pos] = 11;

                if (pos < arr.Count - 1) {
                    Fork(arr_left, pos + 1);
                    Fork(arr_right, pos + 1); 
                }

                TestPermutations(arr_left); // throws exception on success, does nothing on failure
                TestPermutations(arr_right); // throws exception on success, does nothing on failure
            }
            
        }

        private void TestPermutations(List<int> list) {
            int n = list.Count;
            int[] arr = new int[n];

            for (int i = 0; i < n; i++) {
                arr[i] = list[i];
            }

            int m = fakt(n);

            TestOnePermutation(arr);
            for (int i = 0; i < m; i++) {
                NextPermutation(arr);
                TestOnePermutation(arr);
            }
        }

        private void TestOnePermutation(int[] arr) {
            
            int played = (int)Played.Value;
            
            int sum = 0;
            for (int i = 0; i < arr.Length; i++) {
                sum += arr[i];
                if (sum == played) {
                    sum = 0;
                }
            }

            if (sum == 0)
                throw new Exception("permutation found!");
        }

        private static int fakt(int n) {
            if (n < 2)
                return 1;

            return n * fakt(n - 1);
        }

        private static bool NextPermutation(int[] numList)
        {
            /*
             Knuths
             1. Find the largest index j such that a[j] < a[j + 1]. If no such index exists, the permutation is the last permutation.
             2. Find the largest index l such that a[j] < a[l]. Since j + 1 is such an index, l is well defined and satisfies j < l.
             3. Swap a[j] with a[l].
             4. Reverse the sequence from a[j + 1] up to and including the final element a[n].

             */
            var largestIndex = -1;
            for (var i = numList.Length - 2; i >= 0; i--)
            {
                if (numList[i] < numList[i + 1])
                {
                    largestIndex = i;
                    break;
                }
            }

            if (largestIndex < 0) return false;

            var largestIndex2 = -1;
            for (var i = numList.Length - 1; i >= 0; i--)
            {
                if (numList[largestIndex] < numList[i])
                {
                    largestIndex2 = i;
                    break;
                }
            }

            var tmp = numList[largestIndex];
            numList[largestIndex] = numList[largestIndex2];
            numList[largestIndex2] = tmp;

            for (int i = largestIndex + 1, j = numList.Length - 1; i < j; i++, j--)
            {
                tmp = numList[i];
                numList[i] = numList[j];
                numList[j] = tmp;
            }

            return true;
        }

        private List<int> TransMutate() {
            List<int> arr = new List<int>();

            foreach (Card card in this) {
                int x = (int)card.Value;

                arr.Add(x != 11 ? x : 1);
            }

            arr.Sort();

            return arr;
        }
       
        public override void Empty() {
            while (!IsEmpty()) {
                
                Card card = Next();
                this.UnWink(card);
            }
        }

        private void UnWink(Card card) {

            Point point = card.Button.Location;
            card.Button.Location = new Point(point.X, point.Y + 10);
        }
    }
}
