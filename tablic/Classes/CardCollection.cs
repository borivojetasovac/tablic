﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tablic.Classes
{
    public class CardCollection : IEnumerable<Card> {
        protected List<Card> cards;
        //private Table table;
        public int Length { 
            get {
                return cards.Count;
            } 
        }

        public CardCollection() {
            cards = new List<Card>();
        }
       
        public void Shuffle() {

            // homework - shuffle with random sort

            if (cards.Count < 2)
                return;

            Random rand = new Random();

            for (int i = 0; i < 10000; i++) {
                int j;
                Card c1;

                j = rand.Next(0, cards.Count);
                c1 = cards[j];
                cards.RemoveAt(j);

                cards.Add(c1);
            }
        }
        public bool IsEmpty() {
            if (cards.Count == 0)
                return true;

            return false;
        }
        public bool Contains(Card card) {

            if (cards.Contains(card))
                return true;

            return false;
        }
        public Card Peek() {

            return cards[0];
        }
        public void Swap(int i, int j) {
            if (i >= cards.Count || j >= cards.Count)
                throw new Exception("Index out of bounds!");

            Card temp = cards[i];
            cards[i] = cards[j];
            cards[j] = temp;
        }
        
        
        public virtual void Empty() {
            while (!IsEmpty())
                Next();
        }
        public virtual Card Next()
        {
            Card temp;

            if (IsEmpty())
                return null;

            temp = cards[0];
            cards.RemoveAt(0);

            return temp;
        }
        public virtual void Add(Card card) {
            cards.Add(card);
        }
        public virtual Card Remove(Card card) {

            if (!this.Contains(card))
                throw new Exception(card.ToString() + " is not present in this collection! \n" + this.ToString());

            cards.Remove(card);

            return card;
        }

        public override string ToString()
        {
            string s = "";
            foreach (Card card in cards) {
                s += card.ToString() + "\n";
            }

            return s;
        }

        public IEnumerator<Card> GetEnumerator() {
            return this.cards.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public void Sort() {
            cards.Sort();
        }

        public void GetBareCards(CardCollection cc) {
            cards = new List<Card>();

            foreach (Card card in cc)
                cards.Add(new Card(card.Suit, card.Value, card.ImageName));
        }
    }
}
