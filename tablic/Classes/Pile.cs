﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tablic.Classes
{
    public class Pile : CardCollection{
        public Pile() : base() { }

        public override void Add(Card card) {

            if (cards.Count == 52)
                throw new Exception("Can't add 53rd card to the stack!");

            base.Add(card);           
        }        
    }
}
