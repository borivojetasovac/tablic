﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tablic.Classes
{
    public class Table : CardCollection {

        private Form1 ParentForm { get; set; }
        public Point CurrentLocation { get; set; }

        public Table(Form1 parent) : base() {
            ParentForm = parent;
        }

        public override void Add(Card card) {

            if (cards.Count == 52)
                throw new Exception("Can\'t add 53rd card to the table!");

            card.Button.Location = this.CurrentLocation;
            ParentForm.Controls.Add(card.Button);
            card.Button.Click += Button_Click;

            this.CurrentLocation = new Point(this.CurrentLocation.X + 90, this.CurrentLocation.Y);

            base.Add(card);
            // cards.Add(card);
        }
        public override Card Remove(Card card)
        {

            ParentForm.Controls.Remove(card.Button);

            return base.Remove(card);
        }
        public override Card Next() {

            Card card = base.Next();
            ParentForm.Controls.Remove(card.Button);
            return card;
        }

        private void Button_Click(object sender, EventArgs e) {

            Button btn = sender as Button;
            Card chosen = btn.Tag as Card;

            if (!ParentForm.Stash.Contains(chosen)) {
                ParentForm.Stash.Add(this.Wink(chosen));
            }
            else {
                ParentForm.Stash.Remove(this.UnWink(chosen));
            }
            
        }

        private Card UnWink(Card card) { 

            Point point = card.Button.Location;
            card.Button.Location = new Point(point.X, point.Y + 10);

            return card;
        }

        private Card Wink(Card chosen) {

            Point point = chosen.Button.Location;

            chosen.Button.Location = new Point(point.X, point.Y - 10);

            return chosen;
        }

        public void Reorganize() {

            if (this.IsEmpty()) {
                this.CurrentLocation = new Point(120, 140);
                return;
            }

            CardCollection cc = new CardCollection();
            cc.GetBareCards(this);

            while (!this.IsEmpty())
                this.Next();

            this.CurrentLocation = new Point(120, 140);

            while (!cc.IsEmpty())
                this.Add(cc.Next());
        }
    }
}




//private void ButtonProp_Click(object sender, EventArgs e) {
//            Button btn = sender as Button;
//            //Card chosen = btn.Tag as Card;

//            Card chosen5 = new Card(SuitEnum.Clubs, ValueEnum.Ten, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c1.png");
//            ParentForm.StashProp.Add(chosen5);
//            Card chosen = new Card(SuitEnum.Clubs, ValueEnum.Ace, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c1.png");
//            ParentForm.StashProp.Add(chosen);
//            Card chosen4 = new Card(SuitEnum.Clubs, ValueEnum.Three, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c1.png");
//            ParentForm.StashProp.Add(chosen4);
//            Card chosen2 = new Card(SuitEnum.Clubs, ValueEnum.Ace, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c1.png");
//            ParentForm.StashProp.Add(chosen2);
//            Card chosen3 = new Card(SuitEnum.Clubs, ValueEnum.Ace, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c1.png");
//            ParentForm.StashProp.Add(chosen3);            
//        }  