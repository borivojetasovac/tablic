﻿using System.Drawing;
using System.Windows.Forms;

namespace Tablic.Classes {

    public class Player {

        private Form1 ParentForm { get; set; }

        public string Name { get; set; }
        public Label Label { get; set; }
        public int Score() {
            int score = 0;

            foreach (Card card in Pile) {
                score += card.ScoreValue();
            }

            if (Pile.Length > 26)
                score += 3;

            score += Notches;

            return score;
        }

        public bool IsLast { get; set; }

        public Hand Hand { get; set; }
        public Pile Pile { get; set; }
        public int Notches { get; set; }

        public Player Opponent { get; set; }

        public Player(Form1 parent, string name, int ypos) {

            ParentForm = parent;

            Name  = name;
            Notches = 0;

            Pile = new Pile();

            Hand = new Hand(ParentForm);
            Hand.CurrentLocation = new Point(120, ypos);
            Hand.Owner = this;

            Label = new Label();
            Label.AutoSize = true;
            Label.Location = new Point(Hand.CurrentLocation.X - 100, Hand.CurrentLocation.Y);
            Label.Size = new Size(35, 13);
            Label.Text = name;
            this.ParentForm.Controls.Add(Label);
        }

        public void EnableHand() {

            foreach (Card card in Hand) {
                card.Enable();
            }
        }

        public void DisableHand() {

            foreach (Card card in Hand) {
                card.Disable();
            }
        }

        public bool Owns(Card card) {

            if (Hand.Contains(card))
                return true;
            else
                return false;
        }
    }
}
