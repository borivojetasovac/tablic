﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tablic.Classes
{
    public enum SuitEnum {
        Spades,
        Diamonds,
        Hearts,
        Clubs
    };

    public enum ValueEnum {
        Two = 2,
        Three,
        Four,
        Five, 
        Six, 
        Seven, 
        Eight, 
        Nine,
        Ten, 
        Ace, 
        Jack,
        Queen,
        King
    };

    public class Card : IComparable<Card> {

        private static string[] _Suit = { "Spades", "Diamonds", "Hearts", "Clubs" };
        private static string[] _Value = { "", "", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Ace", "Jack", "Queen", "King" };

        // najmodernije
        public SuitEnum Suit { get; private set; }
        public ValueEnum Value { get; private set; }

        public Button Button { get; set; }
        private Image Image { get; set; }

        public string ImageName { get; set; }
        public string BackImageName { get; set; } 

        public Card(SuitEnum s, ValueEnum v, string imageName) {
            this.Suit = s;
            this.Value = v;

            ImageName = imageName;
            // C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets
            BackImageName = @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\b2fv.png";

            Button = new Button();

            Button.Image = new Bitmap(ImageName);
            Button.ImageAlign = ContentAlignment.MiddleCenter;
            Button.Size = new Size(71, 96);
            Button.TextAlign = ContentAlignment.MiddleLeft;
            Button.UseVisualStyleBackColor = true;

            Button.Tag = this;
        }

        public override string ToString() {
            return Card._Value[(int)this.Value] + " of "  + Card._Suit[(int)this.Suit] + " (" + ImageName + ")";
        }

        public static void Swap(ref Card card1, ref Card card2) {
            Card temp = new Card(SuitEnum.Clubs, ValueEnum.Two, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c2.png");

            temp  = card1;
            card1 = card2;
            card2 = temp;
        }

        public int ScoreValue() {
            int index = (int)Value;

            if (index > 9 && !Is(new Card(SuitEnum.Diamonds, ValueEnum.Ten, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\d10.png")))
                return 1;

            if (Is(new Card(SuitEnum.Diamonds, ValueEnum.Ten, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\d10.png")))
                return 2;

            if (Is(new Card(SuitEnum.Clubs, ValueEnum.Two, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c2.png")))
                return 1;

            return 0;
        }

        public bool Is(Card card) {
            if (this.Value == card.Value && this.Suit == card.Suit)
                return true;
            else
                return false;
        }

        public int CompareTo(Card other) {
            if ((int)this.Suit != (int)other.Suit)
                return (int)this.Suit - (int)other.Suit;
            else
                return (int)this.Value - (int)other.Value;
        }

        public void Disable() {

            Button.Image = new Bitmap(BackImageName);
            Button.ImageAlign = ContentAlignment.MiddleCenter;
        }

        public void Enable() {

            Button.Image = new Bitmap(ImageName);
            Button.ImageAlign = ContentAlignment.MiddleCenter;

        }
    }
}
