﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tablic.Classes
{
    public class Hand : CardCollection {

        private Form1 ParentForm { get; set; }
        public Point CurrentLocation { get; set; }
        public Player Owner { get; set; }

        public Hand(Form1 parent) : base() {
            ParentForm = parent;
        }

        public override void Add(Card card) {

            if (cards.Count == 6)
                throw new Exception("Can't add 7th card to the hand!");

            card.Button.Location = this.CurrentLocation;
            ParentForm.Controls.Add(card.Button);
            card.Button.Click += Button_Click;

            this.CurrentLocation = new Point(this.CurrentLocation.X + 90, this.CurrentLocation.Y);

            base.Add(card);
        }
        public override Card Remove(Card card) {

            ParentForm.Controls.Remove(card.Button);
            card.Button.Click -= Button_Click;

            return base.Remove(card);
        }
        public override Card Next() {
            throw new Exception("Don't call Next() on Hand collection!");
        }

        private void Button_Click(object sender, EventArgs e) {

            Button btn = sender as Button;
            Card clicked = btn.Tag as Card;

            if (!ParentForm.CurrentPlayer.Owns(clicked))
                return;

            Card played = clicked;

            if (ParentForm.Stash.IsEmpty()) {
                ParentForm.Table.Add(this.Remove(played));
            }
            else if (ParentForm.Stash.IsCorrect(played)) {

                MessageBox.Show("yey!");
                ParentForm.LastScorer = ParentForm.CurrentPlayer;

                while (!ParentForm.Stash.IsEmpty()) {
                    Card card = ParentForm.Stash.Next();
                    this.Owner.Pile.Add(card);
                    ParentForm.Table.Remove(card);
                }

                this.Owner.Pile.Add(this.Remove(played));
            }
            else { // ! IsCorrect
                MessageBox.Show(ParentForm.Stash.ToString() + "\n" + played.ToString() +  "\nLearn to count!");

                ParentForm.Stash.Empty();
                ParentForm.Table.Add(this.Remove(played));             
            }

            if (ParentForm.Table.IsEmpty())
                ParentForm.CurrentPlayer.Notches++;

            ParentForm.Table.Reorganize();
            ParentForm.ChangeTurn();

            if (this.Owner.IsLast && this.Length == 1) {

                this.Peek().Button.Click += this.ParentForm.Button_Click;
            }
        }        
    }
}


//private void ButtonProp_Click(object sender, EventArgs e) {

//            //Button btn = sender as Button;
//            //Card played = btn.Tag as Card;

//            //if (ParentForm.StashProp.IsEmpty()) {
//            //    ParentForm.Table.Add(this.Remove(played));
//            //    return;
//            //}

//            Card played = new Card(SuitEnum.Diamonds, ValueEnum.Queen, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\c1.png");

//            if (ParentForm.StashProp.IsCorrect(played)) {
//                MessageBox.Show("yey!");
//            }
//        }      