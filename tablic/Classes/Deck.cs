﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tablic.Classes
{
    public class Deck : CardCollection {

        public Deck() : base() {
            int j = 0, k = 2; 
            string[] s  = { "s", "d", "h", "c"};
            string[] v = { "", "", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1", "j", "q", "k"};

            foreach (SuitEnum suit in Enum.GetValues(typeof(SuitEnum))) {
                foreach (ValueEnum value in Enum.GetValues(typeof(ValueEnum))) {
                    cards.Add( new Card(suit, value, @"C:\Users\Borivoje\Desktop\csharp\tablic\tablic\Assets\" + s[j] + v[k++] + ".png") );
                }

                k = 2;
                j++;
            }
        }

        public override void Add(Card card) {
            throw new Exception("Can\'t add a card to the deck, only constructor can!");
        }

        public void AceShuffle() {
            this.Shuffle();

            int pos = this.FirstAcePosition(0);
            this.Swap(3, pos);

            pos = this.FirstAcePosition(4);
            this.Swap(4, pos);
        }

        private int FirstAcePosition(int j) {

            int i = 0;

            foreach (Card card in this) {

                if (i < j) {
                    i++;
                    continue;
                }

                if (card.Value == ValueEnum.Ace)
                    return i;
                i++;
            }

            return -1;
        }
    }
}
