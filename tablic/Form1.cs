﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Tablic.Classes;

namespace Tablic
{
    public partial class Form1 : Form {
        private Deck deck;
        private Player player_1, player_2;
        public Table Table { get; private set; }
        public Stash Stash { get; private set; }
        public Player LastScorer { get; set; }
        public Player CurrentPlayer { get; private set; }

        public Form1() {
            InitializeComponent();

            Stash = new Stash(this);

            deck = new Deck();
            //deck.Shuffle();
            deck.AceShuffle();

            player_1 = new Player(this, "herr zika", 20);
            player_2 = new Player(this, "pera", 260);

            Table = new Table(this);
            Table.CurrentLocation = new Point(120, 140);

            for (int i = 0; i < 4; i++) {
                Table.Add(deck.Next());
            }

            for (int i = 0; i < 6; i++) {
                player_1.Hand.Add(deck.Next());
                player_2.Hand.Add(deck.Next());               
            }

            CurrentPlayer = player_2;
            ChangeTurn();
            player_2.IsLast = true;
            player_1.IsLast = false;
        }

        public void Button_Click(object sender, EventArgs e) {

            Button former_last_swallow = sender as Button;
            former_last_swallow.Click -= Button_Click;

            if (!deck.IsEmpty()) {

                player_1.Hand.CurrentLocation = new Point(120, 20);
                player_2.Hand.CurrentLocation = new Point(120, 260);
                Table.Reorganize();

                for (int i = 0; i < 6; i++) {
                    player_1.Hand.Add(deck.Next());
                    player_2.Hand.Add(deck.Next());
                }

                ChangeTurn();   // just to see cards' backs
                ChangeTurn();
            }
            else {

                while (!Table.IsEmpty()) {
                    Card card = Table.Next();
                    LastScorer.Pile.Add(card);
	            }

                MessageBox.Show(player_1.Name + ": " + player_1.Pile.Length + ", " + player_1.Score() + "\n" + player_2.Name + ": " + player_2.Pile.Length + ", " + player_2.Score());
                CardCollection cc = new CardCollection();
                foreach (Card card in player_1.Pile)
                    cc.Add(card);
                foreach (Card card in player_2.Pile)
                    cc.Add(card);

                cc.Sort();
                MessageBox.Show(cc.ToString());
            }
        }

        public void ChangeTurn() {
            if (CurrentPlayer == player_1) {
                CurrentPlayer = player_2;
                CurrentPlayer.Opponent = player_1;
            }
            else {
                CurrentPlayer = player_1;
                CurrentPlayer.Opponent = player_2;
            }

            CurrentPlayer.EnableHand();
            CurrentPlayer.Opponent.DisableHand();

            //MessageBox.Show(CurrentPlayer.Name + "\'s turn!");
        }
    }
}
